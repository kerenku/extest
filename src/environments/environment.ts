// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDcEeep6zm33U7-QlOXaONHFERHgKA2Avc",
    authDomain: "example-f4e5a.firebaseapp.com",
    databaseURL: "https://example-f4e5a.firebaseio.com",
    projectId: "example-f4e5a",
    storageBucket: "example-f4e5a.appspot.com",
    messagingSenderId: "670757667794",
    appId: "1:670757667794:web:e9ab9e5e0245ca1834fe0b",
    measurementId: "G-0T112FCNCD"
  }
  };
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
