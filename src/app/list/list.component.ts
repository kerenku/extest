import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import {BooksService} from './../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  panelOpenState = false;
  books:any;
  userId:string;
  books$:Observable<any>;

  deleteBook(id:string){
    this.booksservice.deleteBook(id,this.userId);
  }

  constructor(private booksservice:BooksService, public authService:AuthService) { }

  ngOnInit() {
    
this.authService.user.subscribe(
  user=>{
    this.userId=user.uid;
    this.books$ = this.booksservice.getBooks(this.userId);
  }
)
    
   

  }

}
