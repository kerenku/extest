import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  doc:string;
  id:string;
  categoryImage:string; 
  cn:any;
  cas:object[] = [{id:0, name:'business'},{id:1, name:'entertainment'},{id:2, name:'politics'},{id:3, name:'sport'},{id:4, name:'tech'}];
  idd:number;
  userId:string; 

  constructor(public classifyService:ClassifyService,
    public imageService:ImageService,private booksservice:BooksService, 
    private router:Router ,private route:ActivatedRoute,
    public authService :AuthService) {}

              onSubmit(){
                if (this.cn === 'business') {
                  this.idd=0;
                 // alert(this.id);
                }
                else if(this.cn === 'entertainment') {
                  this.idd=1;
                
                }
                else if (this.cn === 'politics') {
                  this.idd=2;
                 
                }
                else if (this.cn === 'sport') {
                  this.idd=3;
                 
                }
                else if (this.cn === 'tech') {
                  this.idd=4;
                 
                }

                console.log(this.idd);
                 this.category = this.classifyService.categories[this.idd];
                 this.categoryImage = this.imageService.images[this.idd];
              }
 
              onSubmit1(){
                this.doc=this.classifyService.doc;
                console.log(this.userId);
                console.log(this.doc);
                console.log(this.category);
                this.booksservice.addBook(this.userId,this.doc,this.category);
                this.router.navigate(['/list']);
              }
              
              ngOnInit() {
              this.classifyService.classify().subscribe(
                res => {
                  console.log(res);
                  console.log(this.classifyService.categories[res])
                  this.category = this.classifyService.categories[res];
                  console.log(this.imageService.images[res]);
                  this.categoryImage = this.imageService.images[res];
                  console.log(this.classifyService.doc)
                }
    )

                // רוצה לדעת מי היוזר שאליו נשמור 
                this.authService.user.subscribe(
                  user=>{
                    this.userId = user.uid;
      
            }
          )
        }
    
      
 
}
